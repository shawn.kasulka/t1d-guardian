/**
 * View Models used by Spring MVC REST controllers.
 */
package com.kasulka.t1dg.web.rest.vm;
